package server

import (
	"context"
	"fmt"

	"github.com/ranggaadi/golang-grpc-api/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/ranggaadi/golang-grpc-api/models"

	student "github.com/ranggaadi/golang-grpc-api/proto"
)

type Server struct{}

//implementasi ListStudent pada server
func (s *Server) ListStudent(req *student.NoParams, stream student.StudentService_ListStudentServer) error {
	data := &models.Student{} //untuk menyimpan data hasil decode

	cursor, err := database.StudentDB.Find(context.Background(), bson.M{}) //melakukan query ke mongo
	if err != nil {
		return status.Errorf(codes.Internal, fmt.Sprintf("Something went wrong when querying data on database: %v", err.Error()))
	}

	for cursor.Next(context.Background()) { //iterasi setiap data pada database
		if err = cursor.Decode(data); err != nil {
			return status.Errorf(codes.Unavailable, fmt.Sprintf("Could not decode data coming from database: %v", err.Error()))
		}

		stream.Send(&student.Student{
			Id:          data.ID.Hex(),
			Name:        data.Name,
			Departement: data.Departement,
			Generation:  data.Generation,
		})
	}

	defer cursor.Close(context.Background()) //menutup koneksi diakhir setelah semua selesai dilakukan

	//mengecek apabila cursor memiliki error
	if err := cursor.Err(); err != nil {
		return status.Errorf(codes.Internal, fmt.Sprintf("Something went wrong (unknown error): %v", err.Error()))
	}

	return nil
}

func (s *Server) GetStudent(ctx context.Context, req *student.StudentQueryId) (*student.Student, error) {
	oid, err := primitive.ObjectIDFromHex(req.GetId())
	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("Couldn't convert to ObjectID: %v", err.Error()),
		)
	}

	returnedData := models.Student{}

	result := database.StudentDB.FindOne(ctx, bson.M{"_id": oid})
	if err := result.Decode(&returnedData); err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Couldn't find student with the following ObjectID %s : %v", req.GetId(), err.Error()),
		)
	}

	return &student.Student{
		Id:          returnedData.ID.Hex(),
		Name:        returnedData.Name,
		Departement: returnedData.Departement,
		Generation:  returnedData.Generation,
	}, nil
}

func (s *Server) CreateStudent(ctx context.Context, req *student.Student) (*student.Student, error) {
	// convert menjadi model student yang nantinya akan dijadikan di konversi ke BSON
	data := models.Student{
		// ID: req.GetId()  dikosongkan dan mongodb akan menggenerate ungique id saat memasukan data ini
		Name:        req.GetName(),
		Departement: req.GetDepartement(),
		Generation:  req.GetGeneration(),
	}

	result, err := database.StudentDB.InsertOne(database.DBCtx, data)
	if err != nil { //check jika ada error
		return nil, status.Errorf( //mengembalikan dari fungsi ini
			codes.Internal,
			fmt.Sprintf("Internal Error: %v", err.Error()),
		)
	}

	//mengambil insertedID dari student yang dibuat dan kemudian (karena datatypenya interface) dirubah ke primitive.ObjectID
	oid := result.InsertedID.(primitive.ObjectID)

	return &student.Student{Id: oid.Hex(), //merubah object id ke string
		Name:        req.GetName(),
		Departement: req.GetDepartement(),
		Generation:  req.GetGeneration()}, nil
}

func (s *Server) UpdateStudent(ctx context.Context, req *student.Student) (*student.Student, error) {
	oid, err := primitive.ObjectIDFromHex(req.GetId())
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Couldn't convert to ObjectID: %v", err.Error()))
	}

	updateData := bson.M{
		"name":        req.GetName(),
		"departement": req.GetDepartement(),
		"generation":  req.GetGeneration(),
	}

	returnedData := models.Student{}

	result := database.StudentDB.FindOneAndUpdate(ctx,
		bson.M{"_id": oid},
		bson.M{"$set": updateData},
		options.FindOneAndUpdate().SetReturnDocument(1)) //mengeset option agar mengembalikan data yang telah diupdate

	if err := result.Decode(&returnedData); err != nil {
		return nil, status.Errorf(codes.Internal,
			fmt.Sprintf("Couldn't find student with the following ObjectID %s : %v", req.GetId(), err.Error()))
	}

	return &student.Student{
		Id:          returnedData.ID.Hex(),
		Name:        returnedData.Name,
		Departement: returnedData.Departement,
		Generation:  returnedData.Generation,
	}, nil
}

func (s *Server) DeleteStudent(ctx context.Context, req *student.StudentQueryId) (*student.Student, error) {
	oid, err := primitive.ObjectIDFromHex(req.GetId())
	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("Couldn't convert given ID to ObjectID: %v", err.Error()),
		)
	}

	deletedStudent := models.Student{}

	result := database.StudentDB.FindOne(ctx, bson.M{"_id": oid})
	if err = result.Decode(&deletedStudent); err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("There is no student with the following ObjectID %s: %v", req.GetId(), err.Error()),
		)
	}

	_, err = database.StudentDB.DeleteOne(ctx, bson.M{"_id": oid})
	if err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("There is no student with the following ObjectID %s: %v", req.GetId(), err.Error()),
		)
	}

	return &student.Student{
		Id:          deletedStudent.ID.Hex(), //perlu dikonversi ke string dengan Hex karena type data pada model adalah primitive.ObjectID
		Name:        deletedStudent.Name,
		Departement: deletedStudent.Departement,
		Generation:  deletedStudent.Generation,
	}, nil

}

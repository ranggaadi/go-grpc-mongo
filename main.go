package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"

	"github.com/ranggaadi/golang-grpc-api/database"
	student "github.com/ranggaadi/golang-grpc-api/proto"
	"github.com/ranggaadi/golang-grpc-api/server"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {

	fmt.Printf("Starting server on port 5000\n")

	listener, err := net.Listen("tcp", ":5000")
	if err != nil {
		log.Fatalf("Unable to listen on port 5000: %v", err.Error())
	}

	server := &server.Server{}
	grpcServer := grpc.NewServer()

	student.RegisterStudentServiceServer(grpcServer, server)
	reflection.Register(grpcServer)

	database.InitializeDB()

	if err = grpcServer.Serve(listener); err != nil {
		log.Fatalf("Something went wrong when serving grpc server: %v", err.Error())
		return
	}
	fmt.Sprintln("Server successfully started on port 5000")

	//SHUTDOWN HOOK
	//membuat channel untuk  menerinma sinyal dari OS
	interruptChan := make(chan os.Signal)

	//menerima interupt (CTRL+C)
	//mengabaikan sinyal lain yang datang
	signal.Notify(interruptChan, os.Interrupt)

	//akan memblock routine hingga sinyal diterima (ctrl+c ditekan)
	<-interruptChan

	fmt.Println("\n Stopping the server...")
	grpcServer.Stop()
	listener.Close()
	fmt.Println("Closing mongodb connection")
	database.DB.Disconnect(database.DBCtx)
	fmt.Println("Successfully Closing terminate all connection")
}

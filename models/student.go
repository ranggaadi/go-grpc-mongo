package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Student struct {
	ID          primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	Name        string             `bson:"name" json:"name" binding:"required"`
	Departement string             `bson:"departement" json:"departement" binding:"required"`
	Generation  uint64             `bson:"generation" json:"generation" binding:"required"`
}

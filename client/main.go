package main

import (
	"log"

	student "github.com/ranggaadi/golang-grpc-api/proto"

	"github.com/ranggaadi/golang-grpc-api/controllers"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error when connecting / dialing to the grpc server : %v", err.Error())
		return
	}

	defer conn.Close()

	client := student.NewStudentServiceClient(conn)

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()

	//Only for testing purposes
	router.GET("/", controllers.HelloWorld())

	v1 := router.Group("/api/v1")
	{
		v1.GET("/students", controllers.GetAllStudent(client))
		v1.GET("/students/:id", controllers.GetStudent(client))
		v1.POST("/students", controllers.CreateStudent(client))
		v1.PUT("/students/:id", controllers.UpdateStudent(client))
		v1.DELETE("/students/:id", controllers.DeleteStudent(client))
	}

	if err := router.Run(":8000"); err != nil {
		log.Fatalf("Error occured when trying to start client server : %v", err.Error())
	}
}

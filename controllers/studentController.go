package controllers

import (
	"io"
	"net/http"

	"github.com/ranggaadi/golang-grpc-api/models"
	"go.mongodb.org/mongo-driver/bson/primitive"

	student "github.com/ranggaadi/golang-grpc-api/proto"

	"github.com/gin-gonic/gin"
)

func HelloWorld() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"result": "Healthy",
		})
	}
}

func GetAllStudent(client student.StudentServiceClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		req := &student.NoParams{}

		var allStudent []models.Student

		stream, err := client.ListStudent(c, req)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		//iterasi melalui stream
		for {

			//stream Recv mengembalikan sebuah pointer ke student.Student pada student.Student saat ini (current)
			res, err := stream.Recv()
			if err == io.EOF { //jika stream sudah habis maka break the loop
				break
			}

			//jika err maka akan mereturn error
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"status":  "fail",
					"message": err.Error(),
				})
				return
			}

			//string ke objectID
			oid, err := primitive.ObjectIDFromHex(res.GetId())
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"status":  "fail",
					"message": "Cannot convert provided id to Object ID",
				})
				return
			}

			allStudent = append(allStudent, models.Student{
				ID:          oid,
				Name:        res.GetName(),
				Departement: res.GetDepartement(),
				Generation:  res.GetGeneration(),
			})
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "success",
			"data":   allStudent,
		})
	}
}

func GetStudent(client student.StudentServiceClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Param("id")

		req := &student.StudentQueryId{Id: id}

		res, err := client.GetStudent(c, req)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		//string ke objectID
		oid, err := primitive.ObjectIDFromHex(res.GetId())
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status":  "fail",
				"message": "Cannot convert provided id to Object ID",
			})
			return
		}

		resStudent := models.Student{
			ID:          oid,
			Name:        res.GetName(),
			Departement: res.GetDepartement(),
			Generation:  res.GetGeneration(),
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "success",
			"data":   resStudent,
		})
	}
}

func CreateStudent(client student.StudentServiceClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		var userInput models.Student
		if err := c.ShouldBindJSON(&userInput); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "fail",
				"message": "Invalid user input",
			})
			return
		}

		req := &student.Student{
			Name:        userInput.Name,
			Departement: userInput.Departement,
			Generation:  userInput.Generation,
		}

		res, err := client.CreateStudent(c, req)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		oid, err := primitive.ObjectIDFromHex(res.GetId())
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status":  "fail",
				"message": "Cannot convert provided id to Object ID",
			})
			return
		}

		resStudent := models.Student{
			ID:          oid,
			Name:        res.GetName(),
			Departement: res.GetDepartement(),
			Generation:  res.GetGeneration(),
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "sucess",
			"data":   resStudent,
		})
	}
}

func UpdateStudent(client student.StudentServiceClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Param("id")

		var userInput models.Student
		if err := c.ShouldBindJSON(&userInput); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "fail",
				"message": "Invalid user input",
			})
			return
		}

		req := &student.Student{
			Id:          id,
			Name:        userInput.Name,
			Departement: userInput.Departement,
			Generation:  userInput.Generation,
		}

		res, err := client.UpdateStudent(c, req)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		oid, err := primitive.ObjectIDFromHex(res.GetId())
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status":  "fail",
				"message": "Cannot convert provided id to Object ID",
			})
			return
		}

		returned := models.Student{
			ID:          oid,
			Name:        res.GetName(),
			Departement: res.GetDepartement(),
			Generation:  res.GetGeneration(),
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "success",
			"data":   returned,
		})
	}
}

func DeleteStudent(client student.StudentServiceClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		req := &student.StudentQueryId{
			Id: c.Param("id"),
		}
		res, err := client.DeleteStudent(c, req)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "fail",
				"message": err.Error(),
			})
			return
		}

		oid, err := primitive.ObjectIDFromHex(res.GetId())
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status":  "fail",
				"message": "Cannot convert provided id to Object ID",
			})
			return
		}

		returned := models.Student{
			ID:          oid,
			Name:        res.GetName(),
			Departement: res.GetDepartement(),
			Generation:  res.GetGeneration(),
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "success",
			"data":   returned,
		})
	}
}

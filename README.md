### How to run

1. Run the grpc service server (/go-grpc-mongo/main.go)
`$ go run main.go`

2. Run the grpc service client (/go-grpc-mongo/client/main.go)
`$ go run client/main.go`

3. Test the endpoints wiht postman (or other API client software)
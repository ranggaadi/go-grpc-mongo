package database

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	DB        *mongo.Client
	StudentDB *mongo.Collection
	DBCtx     context.Context
)

func InitializeDB() {

	//initialize mongodbClient
	fmt.Println("Starting mongoDB connection ...")

	//non-nil empty context
	DBCtx = context.Background()

	DB, err := mongo.Connect(DBCtx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatalf("There is something wrong when connecting to mongo database: %v\n", err.Error())
	}

	err = DB.Ping(DBCtx, nil)
	if err != nil {
		log.Fatalf("Couldn't connect to MongoDB: %v\n", err.Error())
	} else {
		fmt.Println("Successfully connected to database")
	}

	StudentDB = DB.Database("go-grpc-api").Collection("students")
	return
}

module github.com/ranggaadi/golang-grpc-api

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.2
	go.mongodb.org/mongo-driver v1.3.4
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.23.0
)
